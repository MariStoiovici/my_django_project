from django.urls import path, include
from .import views

from django.contrib.auth import views as auth_views

app_name = "brownbag_app"

urlpatterns = [
    path("", views.homepage, name="homepage"),
    path("add_brownbag/", views.add_brownbag, name="add_brownbag"),
]




