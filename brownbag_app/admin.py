from django.contrib import admin
from .models import BrownBag


class BrownBagAdmin(admin.ModelAdmin):
    fields = [
        "title",
        "description",
        "presentation_date",
        "author",
    ]


admin.site.register(BrownBag, BrownBagAdmin)

