from django.contrib.auth import get_user_model
from django.db import models


class BrownBag(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    presentation_date = models.DateField(
        auto_now=False,
        null=True,
        blank=True
    )
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self):
        return self.title



