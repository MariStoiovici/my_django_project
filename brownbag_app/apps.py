from django.apps import AppConfig


class BrownbagAppConfig(AppConfig):
    name = 'brownbag_app'
