
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.response import TemplateResponse
from django.urls import reverse

from brownbag_app.forms import AddBrownBagForm
from .models import BrownBag


def homepage(request):
    """Shows the homepage, which lists all brownbags"""
    brownbags = BrownBag.objects.all()
    return render(
        request=request,
        template_name="brownbag_app/index_home.html",
        context={
            "brownbags": brownbags,
        }
    )


def add_brownbag(request):
    """Shows the form used for creating a brownbag"""

    # If this is a POST request then process the Form data
    if request.method == 'POST':
        form = AddBrownBagForm(request.POST)

        if form.is_valid():
            cleaned_data = form.cleaned_data
            title = cleaned_data['title']
            description = cleaned_data['description']
            presentation_date = cleaned_data['presentation_date']
            author = cleaned_data['author']

            BrownBag.objects.create(
                title=title,
                description=description,
                presentation_date=presentation_date,
                author=author
            )
            return redirect(reverse('brownbag_app:homepage'))

    # If this is a GET (or any other method) create the default form.
    else:
        form = AddBrownBagForm(request.GET)

    context = {
        'form': form,
    }
    return TemplateResponse(
        request=request,
        template="brownbag_app/add_brownbag.html",
        context=context
    )




