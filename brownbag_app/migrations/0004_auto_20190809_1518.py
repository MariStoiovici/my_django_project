# Generated by Django 2.2.1 on 2019-08-09 14:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('brownbag_app', '0003_auto_20190717_1209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='brownbag',
            name='presentation_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
