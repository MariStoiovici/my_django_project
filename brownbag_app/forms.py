from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm
from django.forms import ModelForm

from brownbag_app.models import BrownBag


class AddBrownBagForm(forms.Form):
    title = forms.CharField()
    presentation_date = forms.DateField(
        help_text='dd/mm/yyyy'
    )
    author = forms.ModelChoiceField(
        get_user_model().objects.all()
    )
    description = forms.CharField()


class BrownBagForm(ModelForm):

    class Meta:
        model = BrownBag
        fields = '__all__'


class AuthenticationFormWithInactiveUsersOkay(AuthenticationForm):
    def confirm_login_allowed(self, user):
        pass
